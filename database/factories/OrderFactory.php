<?php

use Faker\Generator as Faker;
use App\User;
use App\Product;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Order::class, function (Faker $faker) {

	$users = User::all()->pluck('id')->toArray();
	$products = Product::all()->pluck('id')->toArray();

    return [
        'user_id' => $faker->randomElement($users),
        'product_id' => $faker->randomElement($products),
        'order_number' => $faker->unique()->randomDigit,
    ];
});
